package com.ccmsd.activity.bot.impl;

import org.springframework.stereotype.Component;

@Component
public class ChatBotService {

	public String getMessage(String msg) {
		String expression=findExpressions(msg);
		switch(expression)
		{
		case "welcome" :
			return wecomeMsg();
		case "help" :
			return helpMsg();
		case "applyNewCredit":
			return msgNewCredit();
		case "blockCredit":
			return msgblockCredit();
		case "applyPremoiumCredit":
			return msgapplyPremoiumCredit();
		default:
			return defaultMsg();
		}
	}

	private String msgapplyPremoiumCredit() {
		StringBuilder msg = new StringBuilder();
		msg.append("Provided with all the document(s) required, it should take approximately 9 days to get your credit card approved.\n");
		return msg.toString();
	}

	private String msgblockCredit() {
		StringBuilder msg = new StringBuilder();
		msg.append("Please call our customer care 1234566789.\n");
		return msg.toString();
	}

	private String msgNewCredit() {
		StringBuilder msg = new StringBuilder();
		msg.append("Provided with all the document(s) required, it should take approximately 9 days to get your credit card approved.\n");
		return msg.toString();
	}

	private String helpMsg() {
		StringBuilder msg = new StringBuilder();
		msg.append("---Help Chat Bot---\n");
		msg.append("Ask me about bank currency,credit cards\n");
		return msg.toString();
	}

	private String defaultMsg() {
		StringBuilder msg = new StringBuilder();
		msg.append("```");
		msg.append("Sorry,\n");
		msg.append("I am not a super bot i can understand only few things.type 'help' to get details\n");
		return msg.toString();
	}

	private String wecomeMsg() {
		StringBuilder welcomeMsg = new StringBuilder();
		welcomeMsg.append("```");
		welcomeMsg.append("Hi  \n");
		welcomeMsg.append("I am a bot to get things done easily, type 'help' to get details \n");
		welcomeMsg.append("```");
		return welcomeMsg.toString();
	}

	String findExpressions(String msg)
	{
		if(msg.equalsIgnoreCase("hi") || msg.equalsIgnoreCase("hello"))
		{
			return "welcome";
		}
		if(msg.equalsIgnoreCase("help") || (msg.length() < 20 && msg.contains("help")) )
		{
			return "help";
		}
		if(msg.contains("credit") && msg.contains("card"))
		{
			if(msg.contains("apply"))
			{
				if(msg.contains("premium"))
				{
					return "applyPremoiumCredit";
				}
				return "applyNewCredit";
			}
			if(msg.contains("block"))
			{
				return "blockCredit";
			}
			return "help";
		}
		return "";
		
		
	}
}
