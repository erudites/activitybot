package com.ccmsd.activity.bot.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;

/**
 * @author Kowsi
 * @version 1.0.0 
 */
@Component
public class ChatBot extends Bot {

	@Autowired
	ChatBotService chatbot;
	@Value("${slackBotToken}")
	private String slackToken;


	@Override
	public Bot getSlackBot() {
		return this;
	}

	@Override
	public String getSlackToken() {
		return slackToken;
	}

	
	@Controller(events = { EventType.DIRECT_MENTION, EventType.DIRECT_MESSAGE })
	public void onReceiveDM(WebSocketSession session, Event event) {
			onMessage(session,event);
	}
	
	

	//@Controller(pattern = "(abot-help*)")
	public void onMessage(WebSocketSession session, Event event) {
		reply(session, event, new Message(chatbot.getMessage(event.getText())));
	}

	

}
